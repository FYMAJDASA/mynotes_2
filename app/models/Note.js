module.exports = function(mongoose) {
    var Schema = mongoose.Schema,
    mongoosePaginate = require('mongoose-paginate');

    var NoteSchema = new Schema({
        title   :    String,
        url :    String,
        content :    String,
        tags    :   [String],
        state   : { type: String, enum: ['new','ongoing','closed','deleted'] ,default : 'new' },
        created : { type: Date, default: Date.now },
        updated : { type: Date, default: Date.now },
        data: [{
            ref: { type: String },
            content: { type: String }
        }],
        rating :     { type: Number, min: 0, max: 100 }
    });

    NoteSchema.plugin(mongoosePaginate);
    module.exports = mongoose.model('Note', NoteSchema);
}
