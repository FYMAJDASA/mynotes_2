module.exports = function(mongoose) {
    var Schema = mongoose.Schema;

    var TestSchema = new Schema({
        name:    String,
        binary:  Buffer,
        living:  Boolean,
        updated: { type: Date, default: Date.now },
        age:     { type: Number, min: 18, max: 65 },
        mixed:   Schema.Types.Mixed,
        _someId: [{type: Schema.Types.ObjectId, ref: 'AnOtherModel'}],
        array:      [],
        ofString:   [String],
        ofNumber:   [Number],
        ofDates:    [Date],
        ofBuffer:   [Buffer],
        ofBoolean:  [Boolean],
        ofMixed:    [Schema.Types.Mixed],
        ofObjectId: [Schema.Types.ObjectId],
        nested: {
            stuff: { type: String, lowercase: true, trim: true }
        },
        enum                    :{ type: String, required: true, enum: ['X','Y']  }
    });

    module.exports = mongoose.model('Test', TestSchema);
}
