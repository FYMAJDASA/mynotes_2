var  _ =           require('underscore'),
    mongoose = require('mongoose'),
    AuthCtrl =  require('../controllers/authCtrl'),
    Response =  require('../helpers/response'),
    Note = mongoose.model('Note');

var routesApi = [
    {
        path: '/api/notes',
        httpMethod: 'GET',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log("GET - /api/notes");
            return req._db.query.exec(function(err,notes){
                if(!err) {
                    return Response.build(res,200,notes);
                } else {
                    return Response.build(res,500,err);
                }
            })
        }]
    },
    {
        path: '/api/notes',
        httpMethod: 'POST',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log('POST - /api/notes',req.body);
            var note = new Note(req.body);
            note.save(function(err) {
                if(err) {
                    return Response.build(res,500,err);
                } else {
                    return Response.build(res,201);
                }
            });

        }]
    },

    {
        path: '/api/notes/:id',
        httpMethod: 'PUT',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log('PUT - /api/notes',req.body);
            return Note.findByIdAndUpdate(req.params.id, req.body, function (err, note) {
                if (err) {
                    return Response.build(res,500,err);
                } else if (!note) {
                    return Response.build(res,404,'Note not found');
                } else {
                    return Response.build(res,200);
                }
            })
        }]
    },
    {
        path: '/api/notes/:id',
        httpMethod: 'GET',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log("GET - /api/notes/:id");
            return Note.findById(req.params.id).exec(function(err, note) {
                if(err) {
                    return Response.build(res,500,err);
                } else if(!note) {
                    return Response.build(res,404,'Note not found');
                } else {
                    return Response.build(res,200,note);
                }
            });
        }]
    },
    {
        path: '/api/notes/:id',
        httpMethod: 'DELETE',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log("DELETE - /api/segments/:id");
            return Note.findByIdAndRemove(req.params.id, function(err) {
                if(err) {
                    return Response.build(res,500,err);
                }  else {
                    return Response.build(res,200);
                }
            });
        }]
    }
]

module.exports = function(app) {

    _.each(routesApi, function(route) {
        route.middleware.unshift(function(req,res,next){buildQuery(req,res,next);});
        function buildQuery(req,res,next){
            console.log('____req.query.pattern', req.query.pattern)
            var conditions      =  ["null","undefined",undefined].indexOf(req.query.conditions)==-1 ? JSON.parse(decodeURIComponent(req.query.conditions)) : {};//{country:req.token.scope[0]}
            req.query.pattern   =  ["null","undefined",undefined].indexOf(req.query.pattern)==-1 ? JSON.parse(decodeURIComponent(req.query.pattern)) : {};//{country:req.token.scope[0]}

            console.log('____req.query.pattern', req.query.pattern)
            var query = Note;
            var pattern = {};

            //========================================= conditions
            if(req.query.pattern)
            {           
                _.each(req.query.pattern,function(value,key){
                    if(Note.schema.paths[key] && value)
                    {
                        if(Note.schema.paths[key].instance=="String")
                            pattern[key] = new RegExp(value, 'ig');
                        else

                            if(Note.schema.paths[key].instance=="Boolean" && value!="")
                                pattern[key] = value;
                            else
                                pattern[key] = value;
                    }
                })
            }
            query = query.find(_.extend(conditions,pattern));
            //========================================= order and sorting

            if(req.query.limit)
            {
                req.query.skip = req.query.skip ? req.query.skip : 0;
                req.query.skip = req.query.page ? req.query.page-1 : req.query.skip;
                query = query.skip(req.query.skip).limit(req.query.limit);
            }
            if(req.query.sort)
            {
                query = query.sort(req.query.sort);
            }
            req._db = { query : query};
            next();
        }
        route.middleware.unshift(function(req,res,next){AuthCtrl.ensureAuthorizedApi(req,res,next,routesApi)});
        var args = _.flatten([route.path, route.middleware]);

        switch(route.httpMethod.toUpperCase()) {
            case 'GET':
                app.get.apply(app, args);
                break;
            case 'POST':
                app.post.apply(app, args);
                break;
            case 'PUT':
                app.put.apply(app, args);
                break;
            case 'DELETE':
                app.delete.apply(app, args);
                break;
            default:
                throw new Error('Invalid HTTP method specified for route ' + route.path);
                break;
        }
    });

}

