var  _ =           require('underscore'),
    mongoose = require('mongoose'),
    AuthCtrl =  require('../controllers/authCtrl'),
    Response =  require('../helpers/response'),
    Example = mongoose.model('Example');

var routesApi = [
    {
        path: '/api/examples',
        httpMethod: 'GET',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log("GET - /api/examples");
            return Example.find().exec(function(err,examples){
                if(!err) {
                    return Response.build(res,200,examples);
                } else {
                    return Response.build(res,500,err);
                }
            })
        }]
    },
    {
        path: '/api/examples',
        httpMethod: 'POST',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log('POST - /api/examples',req.body);
            var example = new Example(req.body);
            example.save(function(err) {
                if(err) {
                    return Response.build(res,500,err);
                } else {
                    return Response.build(res,201);
                }
            });

        }]
    },

    {
        path: '/api/examples/:id',
        httpMethod: 'PUT',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log('PUT - /api/examples',req.body);
            return Example.findByIdAndUpdate(req.params.id, req.body, function (err, example) {
                if (err) {
                    return Response.build(res,500,err);
                } else if (!example) {
                    return Response.build(res,404,'Example not found');
                } else {
                    return Response.build(res,200);
                }
            })
        }]
    },
    {
        path: '/api/examples/:id',
        httpMethod: 'GET',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log("GET - /api/examples/:id");
            return Segment.findById(req.params.id).exec(function(err, example) {
                if(err) {
                    return Response.build(res,500,err);
                } else if(!example) {
                    return Response.build(res,404,'Example not found');
                } else {
                    return Response.build(res,200,example);
                }
            });
        }]
    },
    {
        path: '/api/examples/:id',
        httpMethod: 'DELETE',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log("DELETE - /api/segments/:id");
            return Example.findByIdAndRemove(req.params.id, function(err) {
                if(err) {
                    return Response.build(res,500,err);
                }  else {
                    return Response.build(res,200);
                }
            });
        }]
    },
    {
        path: '/api/segments/company/:id',
        httpMethod: 'GET',
        access: ['ADMIN','SUPER_ADMIN'],
        middleware: [function(req, res) {
            console.log('GET - /api/segments/company/:id');
            Segment.find({company : req.params.id}).populate('company').exec(function(err,segments){
                if(err) {
                    return Response.build(res,500,err.errors);
                }
                return Response.build(res,200,segments);
            })
        }]
    }
]

module.exports = function(app) {

    _.each(routesApi, function(route) {
        route.middleware.unshift(function(req,res,next){AuthCtrl.ensureAuthorizedApi(req,res,next,routesApi)});
        var args = _.flatten([route.path, route.middleware]);

        switch(route.httpMethod.toUpperCase()) {
            case 'GET':
                app.get.apply(app, args);
                break;
            case 'POST':
                app.post.apply(app, args);
                break;
            case 'PUT':
                app.put.apply(app, args);
                break;
            case 'DELETE':
                app.delete.apply(app, args);
                break;
            default:
                throw new Error('Invalid HTTP method specified for route ' + route.path);
                break;
        }
    });

}

