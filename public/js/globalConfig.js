(function (exports) {

    var config = {
        languages: [{
            code: 'en',
            name: 'English'
        }, {
            code: 'fr',
            name: 'French'
        }]
    }
    exports.languages = config.languages;
})(typeof exports === 'undefined' ? this['CONSTANTS'] = {} : exports);