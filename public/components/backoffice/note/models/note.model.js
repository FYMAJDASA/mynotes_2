angular
    .module('MainApp')
    .factory('NoteModel', function(){
        return {
            bootstrap : function(note){
                note.hasRight = function(right){
                    return note.role && note.role.rights.indexOf(right) !== -1;
                }

                note.belongsTo = function(entities, includeSuperAdmin){
                    return (includeSuperAdmin && (note.isSuperAdmin || note.superAdmin)) || (note.entity && entities.indexOf(note.entity.type) !== -1)
                }
                return note;
            }
        }
    });