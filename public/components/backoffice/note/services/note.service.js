angular.module('MainApp')
    .factory('NoteSvc', function($http){
        return  {
            findAll : function(){
                return $http.get('/api/notes');
            },
            findOne : function(id){
                return $http.get('/api/notes/'+id);
            },
            findByEntity : function(type){
                return $http.get('/api/notes/entity/'+type);
            },
            create : function(note){
                return $http.post('/api/notes',note);
            },
            edit : function(note){
                return $http.put('/api/notes/'+note._id,note);
            },
            delete : function(id){
                return $http.delete('/api/notes/'+id);
            },
            paginate : function(page, limit, search){
                console.log('___search_', search)
                return $http.get('/api/notes?page='+page+'&limit='+limit+'&pattern='+JSON.stringify(search.pattern));
            },
            resetPassword : function(data){
                return $http.put('/api/notes/password/reset',data);
            }
        }
    });