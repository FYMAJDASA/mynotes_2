angular
    .module('MainApp')
    .controller('NoteCtrl', function($scope, NoteSvc, $modal, gettextCatalog, $rootScope, Auth, SessionStorageService) {
        $rootScope.tags = [{
            _id : 1 ,
            name : "MongoDB"
        },{
            _id : 2 ,
            name : "Angular"
        }]
        $scope.pagination = {itemsPerPage : 10, currentPage : 1, maxSize : 7};
        $scope.search = {};

        $scope.$watch('search',function(search){
            populateNotes($scope.pagination.currentPage,search);
        },true)

        $scope.$watch('pagination.currentPage',function(page){
            populateNotes(page,$scope.search);
        })

        function populateNotes(page, search){
            NoteSvc.paginate(page,$scope.pagination.itemsPerPage, search).success(function(response){
                $scope.notes = response//.data;
                $scope.pagination.totalItems = 25;//response.count;
            })
        }

        $scope.addNote = function() {
            $scope.editNote();
        }
        $scope.editNote = function(id) {
            var modalInstance = $modal.open({
                templateUrl: 'components/backoffice/note/views/note-edit.html',
                size : 'lg',
                controller: function ($scope, $modalInstance, $timeout) {
                    $scope.note = {};

                    if(id)
                        NoteSvc.findOne(id).success(function(note){
                            $scope.note = note;
                        })


                    $scope.save = function () {
                        var service = id ? NoteSvc.edit : NoteSvc.create;
                        service($scope.note).success(function(){
                            populateNotes(getParentScope().pagination.currentPage,{pattern : getParentScope().search});
                            $modalInstance.dismiss('cancel');
                        }).error(function(response){
                            $scope.duplicate.email = response.error.errmsg.indexOf('email') !== -1;
                        })
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.deleteNote = function(id) {
            var modalInstance = $modal.open({
                templateUrl: 'components/backoffice/note/views/note-delete.html',
                controller: function ($scope, $modalInstance) {

                    NoteSvc.findOne(id).success(function(note){
                        $scope.note = note;
                    })

                    $scope.save = function () {
                        NoteSvc.delete(id).success(function(){
                            populateNotes(getParentScope().pagination.currentPage,{pattern : getParentScope().search});
                            $modalInstance.dismiss('cancel');
                        })
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.showNote = function(id) {
            var modalInstance = $modal.open({
                templateUrl: 'components/backoffice/note/views/note-show.html',
                controller: function ($scope, $modalInstance) {

                    NoteSvc.findOne(id).success(function(note){
                        $scope.note = note;
                    })

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };


        function getParentScope(){
            return $scope;
        }


    })