angular
    .module('MainApp')
    .controller('UserCtrl', function($scope, UserSvc, $modal, gettextCatalog, $rootScope, Auth, SessionStorageService) {

        $scope.pagination = {itemsPerPage : 10, currentPage : 1, maxSize : 7};
        $scope.search = {};

        $scope.$watch('search',function(search){
            populateUsers($scope.pagination.currentPage,search);
        },true)

        $scope.$watch('pagination.currentPage',function(page){
            populateUsers(page,$scope.search);
        })

        function populateUsers(page, search){
            UserSvc.paginate(page,$scope.pagination.itemsPerPage, search).success(function(response){
                $scope.users = response.data;
                $scope.pagination.totalItems = response.count;
            })
        }

        $scope.addUser = function() {
            var modalInstance = $modal.open({
                templateUrl: 'components/backoffice/user/views/user-add.html',
                size : 'lg',
                controller: function ($scope, $modalInstance) {
                    $scope.countries = [];
                    $scope.required = {};
                    $scope.duplicate = {};

                    console.log('CONSTANT.roles',CONSTANTS.roles);
                    console.log('CONSTANT.roles',_.rest(CONSTANTS.roles));
                    $scope.roles =_.rest(CONSTANTS.roles);

                    $scope.save = function () {
                        $scope.required.firstName = !$scope.user.firstName || !$scope.user.firstName.trim();
                        $scope.required.lastName =  !$scope.user.lastName || !$scope.user.lastName.trim();
                        $scope.required.password =  !$scope.user.password || !$scope.user.password.trim();
                        $scope.required.phone =  !$scope.user.phone || !$scope.user.phone.trim();
                        $scope.required.role =  !$scope.user.role;
                        $scope.required.email =  !$scope.user.email || !$scope.user.email.trim() || !isValidEmail($scope.user.email);
                        if (_.find($scope.required, function (v) {
                            return v;
                        })) {
                            return;
                        }
                        $scope.user.role = $scope.user.role.code;
                        UserSvc.create($scope.user).success(function(){
                            populateUsers(getParentScope().pagination.currentPage,{pattern : getParentScope().search});
                            $modalInstance.dismiss('cancel');
                        }).error(function(response){
                            console.log('RESPONSE ERR' , response);
                            $scope.duplicate.email = response.error.err.indexOf('email') !== -1;
                        })
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.editUser = function(id) {
            var modalInstance = $modal.open({
                templateUrl: 'components/backoffice/user/views/user-edit.html',
                size : 'lg',
                controller: function ($scope, $modalInstance, $timeout) {

                    $scope.roles = [
                        {
                            code : "ADMIN",
                            name : "Administrateur"
                        },
                        {
                            code : "USER",
                            name : "Utilisateur"
                        }
                    ];

                    UserSvc.findOne(id).success(function(user){
                        delete user.password;
                        $scope.user = user;
                    })

                    $scope.required = {};
                    $scope.duplicate = {};


                    $scope.save = function () {
                        $scope.required.firstName = !$scope.user.firstName || !$scope.user.firstName.trim();
                        $scope.required.lastName =  !$scope.user.lastName || !$scope.user.lastName.trim();
                        $scope.required.phone =  !$scope.user.phone || !$scope.user.phone.trim();
                        $scope.required.role =  !$scope.user.role;
                        $scope.required.email =  !$scope.user.email || !$scope.user.email.trim() || !isValidEmail($scope.user.email);
                        if (_.find($scope.required, function (v) {
                            return v;
                        })) {
                            return;
                        }
                        $scope.user.role = $scope.user.role.code;
                        UserSvc.edit($scope.user).success(function(){
                            populateUsers(getParentScope().pagination.currentPage,{pattern : getParentScope().search});
                            $modalInstance.dismiss('cancel');
                        }).error(function(response){
                            $scope.duplicate.email = response.error.errmsg.indexOf('email') !== -1;
                        })
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.deleteUser = function(id) {
            var modalInstance = $modal.open({
                templateUrl: 'components/backoffice/user/views/user-delete.html',
                controller: function ($scope, $modalInstance) {

                    UserSvc.findOne(id).success(function(user){
                        $scope.user = user;
                    })

                    $scope.save = function () {
                        UserSvc.delete(id).success(function(){
                            populateUsers(getParentScope().pagination.currentPage,{pattern : getParentScope().search});
                            $modalInstance.dismiss('cancel');
                        })
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.showUser = function(id) {
            var modalInstance = $modal.open({
                templateUrl: 'components/backoffice/user/views/user-show.html',
                controller: function ($scope, $modalInstance) {

                    UserSvc.findOne(id).success(function(user){
                        $scope.user = user;
                    })

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };


        function getParentScope(){
            return $scope;
        }


    })