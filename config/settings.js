module.exports = {
  "superAdmin": {
    "email": "a",
    "password": "a",
    "firstName": "super",
    "lastName": "admin"
  },
  "server": {
    "node": {
      "port": 4000,
      "oldFile": "stack.js",
      "newFile": "stack.js"
    },
    "redis": {
      "host": process.env.REDIS_1_ADDR || 'localhost',
      "port": process.env.REDIS_1_PORT || 6379,
    }
	
  },
  "seed": false,
  "mail": {
    "selectedOption": "gmail",
    "options": {
      "gmail": {
        "service": "Gmail",
        "from": "dashboard@irevolution.fr",
        "auth": {
          "user": "dashboard@irevolution.fr",
          "pass": "irevolution2013"
        },
        "secureConnection": false
      },
      "smtp": {
        "host": "",
        "from": ""
      }
    }
  }
}